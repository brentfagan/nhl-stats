#!/usr/bin/env bash

python_interpreter=''
python_file=''
conf_file=''
app_setup=''
verbose='false'

print_usage() {
  printf "Usage: ..."
}

while getopts 'i:f:c:a:v' flag; do
  case "${flag}" in
    i) python_interpreter="${OPTARG}" ;;
    f) python_file="${OPTARG}" ;;
    c) conf_file="${OPTARG}" ;;
    a) app_setup="${OPTARG}" ;;
    v) verbose='true' ;;
    *) print_usage
       exit 1 ;;
  esac
done

# cron job to update conf_file - one time set up
(crontab -l ; echo "29 6 * * * $python_interpreter $python_file $conf_file") | crontab -
# cron job to call app_schedule.sh - one time set up
(crontab -l ; echo "0 12 * * * $app_setup") | crontab -