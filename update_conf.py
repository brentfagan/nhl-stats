import json
import sys
from time import strftime


def main(conf_file):
    with open(conf_file, 'r') as f:
        data = json.load(f)

    data['game_date'] = strftime('%Y-%m-%d')

    with open(conf_file, 'w') as f:
        json.dump(data, f)

if __name__ == '__main__':
    file = sys.argv[1]
    main(file)
