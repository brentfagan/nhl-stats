import requests
from requests.auth import HTTPBasicAuth
from time import time


class Authorization:
    def __init__(self, client_id, secret, username, pwd):
        self.client_id = client_id
        self.secret = secret
        self.username = username
        self.pwd = pwd
        self.auth_time = None

    def auth_headers(self):
        client_auth = HTTPBasicAuth(self.client_id, self.secret)
        post_data = {"grant_type": "password", "username": self.username, "password": self.pwd}
        headers = {"User-Agent": "ChangeMeClient/0.1 by {}".format(self.username)}
        try:
            response = requests.post("https://www.reddit.com/api/v1/access_token", auth=client_auth, data=post_data,
                                     headers=headers)
            tokens = response.json()
            headers = {"Authorization": '{} {}'.format(tokens['token_type'], tokens['access_token']),
                       "User-Agent": "gdtpost 0.1 by {}".format(self.username)}
            self.auth_time = time()
        except Exception as e:
            print(e)
        return headers
