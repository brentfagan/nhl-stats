from time import time, sleep, strftime
import os
import signal
import argparse
import warnings
import json
from pprint import pprint

from authorization import Authorization
from videoposter import VideoPoster


def main(base_url, game_date, client_id, client_secret, reddit_username,
         reddit_pwd, search_query, psql_conn_string):
    with open('test_log.txt', 'w') as f:
        f.write('Process ID: {}'.format(os.getpid()))
    auth = Authorization(client_id, client_secret,
                         reddit_username, reddit_pwd)
    authorization_headers = auth.auth_headers()
    token_time = auth.auth_time
    vp = VideoPoster(base_url, game_date, authorization_headers,
                     psql_conn_string)
    loop_flag = True

    while loop_flag:
        start_time = time()
        flag = start_time - token_time < 3400
        if not flag:
            auth = Authorization(client_id, client_secret,
                                 reddit_username, reddit_pwd)
            authorization_headers = auth.auth_headers()
            token_time = auth.auth_time
            vp.headers = authorization_headers
            loop_flag = True
        else:
            loop_flag = flag
        vp.content_links()
        vp.find_thread(search_query)
        pprint(vp.thread_list)
        vp.video_highlights()
        pprint(vp.game_list)
        vp.game_thread_text()
        vp.make_post()
        print(strftime('%H:%M:%S'))
        pprint(vp.game_list)
        num_posted = sum([d['posted'] for d in vp.game_list])
        print('Number of Games: {} ---- Number Posted: {}'.format(vp.num_games,num_posted))
        print('******************************************************************************')
        if num_posted == vp.num_games:
            try:
                print('App shutdown: ', strftime('%H:%M:%S'))
                os.kill(os.getpid(), signal.SIGKILL)
            except Exception as e:
                print(e)
        else:
            # TODO optimize wait times for posting
            sleep(60 * 5)

if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument("-c", "--conf", required=True,
                    help="path to the JSON configuration file")
    args = vars(ap.parse_args())

    warnings.filterwarnings("ignore")
    conf = json.load(open(args["conf"]))
    main(**conf)


