import requests
import re
import json
from datetime import datetime
from pprint import pprint
from time import sleep

from utils import post_text, insert_post_id, get_post_ids


class VideoPoster:
    def __init__(self, base_url, game_date, headers, conn_string):
        self.base_url = base_url
        self.game_date = game_date
        self.headers = headers
        self.conn_string = conn_string
        self.game_list = []
        self.thread_list = []
        self.num_games = None

    def content_links(self):
        games = requests.get(self.base_url + r'/api/v1/schedule?date={}'.format(self.game_date))
        data = games.json()['dates'][0]['games']
        self.num_games = len(data)
        for game in data:
            status = game['status']['detailedState']
            content_link = game['content']['link']
            if status == 'Final' and not any(d['content_link'] == content_link for d in self.game_list):
                game_dict = {
                    'posted': False,
                    'status': status,
                    'content_link': content_link,
                    'home_team': game['teams']['home']['team']['name'],
                    'away_team': game['teams']['away']['team']['name']
                }
                self.game_list.append(game_dict)

    def video_highlights(self):
        for game_dict in self.game_list:
            highlights = []
            scoring = []
            content = requests.get('{}{}'.format(self.base_url, game_dict['content_link']))
            data = content.json()['media']
            for milestone in data['milestones']['items']:
                if milestone['type'] == 'GOAL' and milestone['highlight']:
                    for sub_dict in milestone['highlight']['playbacks']:
                        if sub_dict['name'] == 'FLASH_1800K_960X540':
                            temp_dict = {}
                            temp_dict[milestone['highlight']['title']] = (milestone['statsEventId'],
                                                                          sub_dict['url'])
                            scoring.append(temp_dict)
            for item in data['epg']:
                for sub_dict in item['items']:
                    if 'blurb' in sub_dict:
                        for sub_sub_dict in sub_dict['playbacks']:
                            if sub_sub_dict['name'] == 'FLASH_1800K_960X540':
                                temp_dict = {}
                                temp_dict[sub_dict['blurb']] = sub_sub_dict['url']
                                highlights.append(temp_dict)
            game_dict['scoring'] = scoring
            game_dict['highlights'] = highlights

    def find_thread(self, query):
        thread_list = []
        search_results = requests.get(
            "https://oauth.reddit.com/r/hockey/search.json?q=title:{}&sort=new&restrict_sr=on".format(query),
            headers=self.headers
        )
        for result in search_results.json()['data']['children']:
            title = result['data']['title']
            if title.lower().startswith('post game thread'):
                date = re.search(r'(\d{2} [a-z]{3} \d{4})', title.lower()).group(1)
                date = datetime.strptime(date, '%d %b %Y').strftime('%Y-%m-%d')
                if date == self.game_date:
                    thread_data = {
                        'date': date,
                        'title': title,
                        'id': result['data']['id']
                    }
                    if not thread_list:
                        thread_list.append(thread_data)
                    if not any(d['id'] == thread_data['id'] for d in thread_list):
                        thread_list.append(thread_data)
                    else:
                        continue
        self.thread_list = thread_list

    def game_thread_text(self):
        if self.game_list:
            for game in self.game_list:
                if game['highlights']:
                    if game['home_team'] == 'Montréal Canadiens':
                        game['home_team'] = 'Montreal Canadiens'
                    game_id = [{'id': d['id']} for d in self.thread_list if game['home_team'] in d['title']]
                    if game_id:
                        game.update(game_id[0])
                        text = post_text(game)
                        game.update({'text': text})

    def make_post(self):
        comment_url = 'https://oauth.reddit.com/api/comment'
        for game in self.game_list:
            if len(game['highlights']) == 2:
                thread_id = game['id']
                text = game['text']
                prev_posts = get_post_ids(self.game_date, self.conn_string)
                if thread_id not in prev_posts:
                    try:
                        post = requests.post(
                            '{}?thing_id=t3_{}&text={}'.format(comment_url, thread_id, text.strip()),
                            headers=self.headers
                        )
                        response = post.content.decode('utf-8')
                        if json.loads(response)['success'] is True:
                            print('Posted!')
                            insert_post_id(thread_id, self.game_date, self.conn_string)
                            game['posted'] = True
                        sleep(60 * 10)
                    except Exception as e:
                        print(e, post.status_code, post.reason)
                else:
                    continue
            else:
                continue
