from time import strftime
import requests
import datetime
import pytz
import sys


def main(time_file):
    current_day = strftime('%Y-%m-%d')
    games = requests.get(r'https://statsapi.web.nhl.com/api/v1/schedule?date={}'.format(current_day))
    if games.json()['totalGames'] == 0:
        pass
    else:
        data = games.json()['dates'][0]['games']
        game_times = []
        for game in data:
            est = pytz.timezone('US/Eastern')
            d = datetime.datetime.strptime(game['gameDate'], "%Y-%m-%dT%H:%M:%SZ")
            est_d = d.replace(tzinfo=pytz.utc).astimezone(est)
            game_times.append(est_d)

        start_time = int(str(min(game_times)).split(' ')[1].split(':')[0]) - 9
        with open(time_file, 'w') as f:
            f.write(str(start_time))

if __name__ == '__main__':
    file = sys.argv[1]
    main(file)
