#!/usr/bin/env bash

python_interpreter=''
python_file=''
time_file=''
app=''
verbose='false'

print_usage() {
  printf "Usage: ..."
}

while getopts 'i:f:t:a:v' flag; do
  case "${flag}" in
    i) python_interpreter="${OPTARG}" ;;
    f) python_file="${OPTARG}" ;;
    t) time_file="${OPTARG}" ;;
    a) app="${OPTARG}" ;;
    v) verbose='true' ;;
    *) print_usage
       exit 1 ;;
  esac
done

${python_interpreter} ${python_file} ${time_file}
script_start="$(head -1 ${time_file})"
#this job needs to be updated daily - is specified by the scheduler shell script
echo "$python_interpreter $app" | now + "$script_start" hours