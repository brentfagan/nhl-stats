from collections import namedtuple

from sqlalchemy import create_engine


def markdown_text(string_key, string_value):
    return '[' + string_key + ']' + '(' + string_value + ')' + '  \n\n'


def post_text(game):
    Scoring = namedtuple('Scoring', 'ind name clip')
    scoring_text = []
    highlight_text = []
    for k, v in game.items():
        if k == 'scoring':
            new_list = []
            for scoring_dict in v:
                for title, ind_clip in scoring_dict.items():
                    new_list.append(Scoring(int(ind_clip[0]), title, ind_clip[1]))
            sorted_list = sorted(new_list, key=lambda x: x.ind)
            for elem in sorted_list:
                scoring_text.append(markdown_text(elem[1], elem[2]))
        elif k == 'highlights':
            for highlight in v:
                for title, clip in highlight.items():
                    highlight_text.append(markdown_text(title, clip))
    scoring_text = ''.join(x for x in scoring_text)
    scoring_text = 'Scoring Summary  \n\n' + scoring_text
    highlight_text = ''.join(x for x in highlight_text)
    highlight_text = 'Game Highlights  \n\n' + highlight_text
    all_text = scoring_text + '  \n\n\n' + highlight_text + "  \n\n\n^(P.S. I'm a bot)"
    return all_text


def insert_post_id(post_id, game_date, conn_string):
    sql = """INSERT INTO posts(post_id, game_date) VALUES(%s, %s);"""
    engine = create_engine(conn_string)
    conn = engine.connect()
    trans = conn.begin()
    try:
        conn.execute(sql, post_id, game_date)
        trans.commit()
    except Exception as e:
        trans.rollback()
        print(e)
    conn.close()


def get_post_ids(game_date, conn_string):
    posts_list = []
    engine = create_engine(conn_string)
    conn = engine.connect()
    trans = conn.begin()
    if not engine.dialect.has_table(engine, 'posts'):
        try:
            conn.execute(
                "CREATE TABLE posts (id serial PRIMARY KEY, post_id varchar, game_date date);"
            )
            trans.commit()
        except Exception as e:
            print(e)
    # result = conn.execute('SELECT post_id FROM posts where game_date = {};'.format(game_date))
    result = conn.execute('SELECT post_id FROM posts;')
    for row in result:
        posts_list.append(row['post_id'])
    conn.close()
    return posts_list
